<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreationProductsOrdersPagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('alias');
            $table->double('price');
            $table->text('description');
        });

        Schema::create('orders', function (Blueprint $table){
            $table->increments('id');
            $table->string('customer_name');
            $table->string('email');
            $table->string('phone');
            $table->text('feedback');
        });

        Schema::create('pages', function (Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('alias');
            $table->text('intro');
            $table->text('content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('pages');
    }
}
