<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "ProductsController@index");
Route::get('/create', "ProductsController@create");
Route::get('/{product}/edit', "ProductsController@edit");
Route::post('/{product}', "ProductsController@update");
Route::delete('/{product}', "ProductsController@delete");
Route::put('/', "ProductsController@store");

Route::get('/orders', "OrdersController@index");

Route::get('/pages', "PagesController@index");
Route::get('/pages/create', "PagesController@create");
Route::get('/pages/{post}', "PagesController@show");
Route::get('/pages/{post}/edit', "PagesController@edit");
Route::post('/pages/{post}', "PagesController@update");
Route::delete('/pages/{post}', "PagesController@delete");
Route::put('/pages', "PagesController@store");


