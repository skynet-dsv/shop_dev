<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;


class ProductsController extends Controller
{

    public function index()
    {
        $data['products'] = Product::all();
        return view('index')->with($data);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'price' => 'required',
            'description' => 'required'
        ]);
        Product::create(request()->all());

        return redirect('/');
    }

    public function edit(Product $product)
    {
        return view('products.edit')->with(compact('product'));
    }

    public function update(Product $product)
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'price' => 'required',
            'description' => 'required'
        ]);
        $product->update(request()->all());
        return redirect('/');
    }

    public function delete(Product $product)
    {
        $product->delete();
        return redirect('/');
    }
}
