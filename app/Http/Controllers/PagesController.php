<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;


class PagesController extends Controller
{
    public function index()
    {
        $data['blocks'] = Page::all();
        return view('pages')->with($data);
    }

    public function show(Page $post)
    {
        $data['posts'] = $post;
        return view('posts.show')->with($data);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);
        Page::create(request()->all());

        return redirect('/pages');
    }

    public function edit(Page $post)
    {
        return view('posts.edit')->with(compact('post'));
    }

    public function update(Page $post)
    {
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);
        $post->update(request()->all());
        return redirect('/pages');
    }

    public function delete(Page $post)
    {
        $post->delete();
        return redirect('/pages');
    }
}
