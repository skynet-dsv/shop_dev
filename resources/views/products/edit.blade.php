
@extends('layouts.layout')

@section('navBlock')
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Products <span class="sr-only">(current)</span></a></li>
            <li><a href="/orders">Orders</a></li>
            <li><a href="/pages">Publication</a></li>
        </ul>
        <form class="navbar-form navbar-right">
            <div class="form-group">
                <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
        </form>
    </div><!--/.navbar-collapse -->
@endsection

@section('headerBlock')
    <div class="container">
        <h1 class="text-center">Edit Pproduct</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="/{{$product->id}}">

            {{csrf_field()}} <!-- use token for validation -->

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input name="title" value="{{$product->title}}" type="text" id="title" class="form-control">
                </div>
                <div class="form-group">
                    <label for="alias">Alias:</label>
                    <input name="alias" value="{{$product->alias}}" type="text" id="alias" class="form-control">
                </div>
                <div class="form-group">
                    <label for="price">Price:</label>
                    <input name="price" value="{{$product->price}}" type="text" id="price" class="form-control">
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea name="description" id="description" class="form-control">{{$product->description}}</textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>
            @include('layouts.formsError')
        </div>
    </div>
@endsection