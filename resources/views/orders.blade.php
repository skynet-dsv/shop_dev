@extends('layouts.layout')

@section('navBlock')
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="/">Products</a></li>
            <li class="active"><a href="/orders">Orders <span class="sr-only">(current)</span></a></li>
            <li><a href="/pages">Publication</a></li>
        </ul>
        <form class="navbar-form navbar-right">
            <div class="form-group">
                <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
        </form>
    </div><!--/.navbar-collapse -->
@endsection

@section('headerBlock')
    <div class="container">
        <h1 class="text-center">Orders List</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Customer Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Feedback</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders AS $order)
                    <tr>
                        <th scope="row">{{$order->id}}</th>
                        <td>{{$order->customer_name}}</td>
                        <td>{{$order->email}}</td>
                        <td>{{$order->phone}} Uah</td>
                        <td>{{$order->feedback}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection