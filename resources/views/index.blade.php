@extends('layouts.layout')

@section('navBlock')
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="active" class="dropdown">
                <a href="/" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">Products <span class="caret"><span class="sr-only">(current)</span></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/create">Create Product</a></li>
                </ul>
            </li>
            <li><a href="/orders">Orders</a></li>
            <li><a href="/pages">Publication</a></li>
        </ul>
        <form class="navbar-form navbar-right">
            <div class="form-group">
                <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
        </form>
    </div><!--/.navbar-collapse -->
@endsection

@section('headerBlock')
    <div class="container">
        <h1 class="text-center">Products List</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th width="10 px">id</th>
                    <th width="200 px">Title</th>
                    <th width="90 px">Alias</th>
                    <th width="90 px">Price</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products AS $product)
                    <tr>
                        <th scope="row">{{$product->id}}</th>
                        <td>{{$product->title}}</td>
                        <td>{{$product->alias}}</td>
                        <td>{{$product->price}} Uah</td>
                        <td>{{$product->description}}</td>
                        <td><p><a class="btn btn-warning" href="/{{$product->id}}/edit" role="button">Edit»</a></p>
                            <form action="/{{$product->id}}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                                <input class="btn btn-warning" type="submit" value="Delete»">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection