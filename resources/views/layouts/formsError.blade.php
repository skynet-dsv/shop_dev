@if(count($errors))
    <div class="form-group">
        <div class="alert alert-danger">
            @foreach($errors->all() AS $error)
                <li>{{$error}}
            @endforeach
        </div>
    </div>
@endif