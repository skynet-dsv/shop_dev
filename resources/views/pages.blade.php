@extends('layouts.layout')

@section('navBlock')
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="/">Products</a></li>
            <li><a href="/orders">Orders</a></li>
            <li class="active" class="dropdown">
                <a href="/pages" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">Publication <span class="caret"><span class="sr-only">(current)</span></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/pages/create">Create Page</a></li>
                </ul>
            </li>
        </ul>
        <form class="navbar-form navbar-right">
            <div class="form-group">
                <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
        </form>
    </div><!--/.navbar-collapse -->
@endsection

@section('headerBlock')
    <div class="container">
        <h1 class="text-center">Publication</h1>
    </div>
@endsection

@section('content')
    @foreach($blocks AS $block)
        <div class="col-md-4">
            <h3>{{$block->title}}</h3>
            <p>{{$block->intro}}</p>
            <p><a class="btn btn-default" href="/pages/{{$block->alias}}" role="button">View more »</a></p>
        </div>
    @endforeach
@endsection