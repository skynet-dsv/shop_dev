@extends('layouts.layout')

@section('navBlock')
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="/">Products</a></li>
            <li><a href="/orders">Orders</a></li>
            <li class="active"><a href="/pages">Publication  <span class="sr-only">(current)</span></a></li>
        </ul>
        <form class="navbar-form navbar-right">
            <div class="form-group">
                <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
        </form>
    </div><!--/.navbar-collapse -->
@endsection

@section('headerBlock')
    <div class="container">
        <h1 class="text-center">Create new Page</h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="/pages">

                <input type="hidden" name="_method" value="PUT">

            {{csrf_field()}} <!-- use token for validation -->

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input name="title" type="text" id="title" class="form-control">
                </div>
                <div class="form-group">
                    <label for="alias">Alias:</label>
                    <input name="alias" type="text" id="alias" class="form-control">
                </div>
                <div class="form-group">
                    <label for="intro">Intro:</label>
                    <input name="intro" type="text" id="intro" class="form-control">
                </div>
                <div class="form-group">
                    <label for="content">Content:</label>
                    <textarea name="content" id="content" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Submit</button>
                </div>
            </form>
            @include('layouts.formsError')
        </div>
    </div>
@endsection