@extends('layouts.layout')

@section('navBlock')
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="/">Products</a></li>
            <li><a href="/orders">Orders</a></li>
            <li class="active"><a href="/pages">Publication <span class="sr-only">(current)</span></a></li>
        </ul>
        <form class="navbar-form navbar-right">
            <div class="form-group">
                <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
        </form>
    </div><!--/.navbar-collapse -->
@endsection

@section('headerBlock')
    <div class="container">
        <h3 class="text-center">{{$posts->intro}}</h3>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <p><b><i>{{$posts->title}}</i></b></p>
            <p>{{$posts->content}}</p>
        </div>
        <div class="text-center">
            <p><a class="btn btn-warning" href="/pages/{{$posts->alias}}/edit" role="button">Edit»</a></p>
            <form action="/pages/{{$posts->alias}}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="DELETE">
                <input class="btn btn-warning" type="submit" value="Delete»">
            </form>
        </div>
    </div>
@endsection